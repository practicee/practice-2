using Microsoft.EntityFrameworkCore;
using practice.Models;

public class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
    {
    }

    public DbSet<Student> Students { get; set; }

    public override int SaveChanges()
    {
        // Логика сохранения изменений в базе данных
        return base.SaveChanges();
    }
}
