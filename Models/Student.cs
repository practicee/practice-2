using System;
using System.ComponentModel.DataAnnotations;

namespace practice.Models;

public class Student
{
    public int Id { get; set; }

    [Required(ErrorMessage = "Фамилия обязательна")]
    [RegularExpression(@"^[А-ЯЁ][а-яё]*$", ErrorMessage = "Некорректная фамилия")]
    public string LastName { get; set; }

    [Required(ErrorMessage = "Имя обязательно")]
    [RegularExpression(@"^[А-ЯЁ][а-яё]*$", ErrorMessage = "Некорректное имя")]
    public string FirstName { get; set; }

    [RegularExpression(@"^[А-ЯЁ][а-яё]*$", ErrorMessage = "Некорректное отчество")]
    public string MiddleName { get; set; }

    [Required(ErrorMessage = "Фамилия на латинице обязательна")]
    [RegularExpression(@"^[A-Z][a-z]*$", ErrorMessage = "Некорректная фамилия на латинице")]
    public string LatinLastName { get; set; }

    [Required(ErrorMessage = "Имя на латинице обязательно")]
    [RegularExpression(@"^[A-Z][a-z]*$", ErrorMessage = "Некорректное имя на латинице")]
    public string LatinFirstName { get; set; }

    [RegularExpression(@"^[A-Z][a-z]*$", ErrorMessage = "Некорректное отчество на латинице")]
    public string LatinMiddleName { get; set; }

    [DataType(DataType.Date)]
    public DateTime DateOfBirth { get; set; }

    public string Gender { get; set; }

    [Required(ErrorMessage = "ИИН обязателен")]
    [RegularExpression(@"^\d{12}$", ErrorMessage = "ИИН должен содержать 12 цифр")]
    [CustomValidation(typeof(Student), "ValidateIIN")]
    public string IIN { get; set; }

    public string Citizenship { get; set; }

    public string Nationality { get; set; }

    public string Address { get; set; }

    [RegularExpression(@"\d{9}", ErrorMessage = "Неверный формат мобильного номера")]
    public string MobileNumber { get; set; }

    [EmailAddress(ErrorMessage = "Неверный формат Email")]
    public string Email { get; set; }

    [RegularExpression(@"\d{9}", ErrorMessage = "Неверный формат BIK кода")]
    public string BankBIK { get; set; }

    [RegularExpression(@"[A-Z]{2}\d{18}", ErrorMessage = "Неверный формат IBAN номера счета банка")]
    public string BankIBAN { get; set; }

    public static ValidationResult ValidateIIN(string iin, ValidationContext validationContext)
    {
        // Проверка на корректность ИИН согласно указанным требованиям
        int year = int.Parse(iin.Substring(0, 2));
        int month = int.Parse(iin.Substring(2, 2));
        int day = int.Parse(iin.Substring(4, 2));

        if (year < 0 || year > 99 || month < 1 || month > 12 || day < 1 || day > 31)
        {
            return new ValidationResult("Некорректная дата рождения в ИИН");
        }

        return ValidationResult.Success;
    }
}
