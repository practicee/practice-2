using System;
using Microsoft.AspNetCore.Mvc;
using practice.Models;
using System.Diagnostics;

public class StudentController : Controller
{
    private readonly ApplicationDbContext _context;

    public StudentController(ApplicationDbContext context)
    {
        _context = context;
    }

    public ActionResult Index()
    {
        return View();
    }

    [HttpPost]
    public ActionResult Register(Student student)
    {
        if (ModelState.IsValid)
        {
            // Добавить студента в базу данных
            _context.Students.Add(student);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }

        return View("Index", student);
    }
}
